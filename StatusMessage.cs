﻿using System;

namespace ComTerminalProject
{
    public class StatusMessage
    {
        private byte _configurationStatus;
        private byte _transmisionStatus;
        private byte _receptionStatus;
        private byte _activeLayer;
        private byte _overcurrentFlag;
        private byte _estimatedST7580Temperature;

        public StatusMessage()
        {
        }

        public void Set(byte message) {
            _configurationStatus = (byte) ((byte) (0x80 & message) >> 7);
            _transmisionStatus = (byte) ((byte) (0x40 & message) >> 6);
            _receptionStatus = (byte) ((byte) (0x20 & message) >> 5);
            _activeLayer = (byte) ((byte) (0x18 & message) >> 3);
            _overcurrentFlag = (byte) ((byte) (0x04 & message) >> 2);
            _estimatedST7580Temperature = (byte) (0x03 & message);
        }

        public string ToWrite()
        {
            String message = "STATUS MESSAGE:";
            
            message += "\n\tConfiguration status :       \t\t" + ((_configurationStatus == 0) 
                           ? "autoreconfiguration correctly occurred"
                           :"autoreconfiguration occurred with errors or at least one among " 
                            +"MIB objects 00h (Modem Config), " 
                            +"01h (PHY Config), " 
                            +"02h (SS Key) has not changed its default value after boot");

            message += "\n\tTransmission status :        \t\t" + ((_transmisionStatus == 0)
                           ? "the ST7580 is not transmitting a power line frame"
                           : "the ST7580 is transmitting a power line frame");
            
            message += "\n\tReception status :         \t\t" + ((_receptionStatus == 0) 
                           ?"the ST7580 is not receiving a power line frame " 
                           :"the ST7580 is receiving a power line frame"); 
            
            message += "\n\tActive layer :               \t\t" + ((_activeLayer == 0)
                           ?"PHY layer"
                           :(_activeLayer == 1)
                               ?"DL layer"
                               :(_activeLayer == 2)
                                   ?"SS layer"
                                   :"ST7580 not configured");

            message += "\n\tOvercurrent flag :           \t\t" + ((_overcurrentFlag == 0)
                           ? "no overcurrent event on last transmission"
                           : "last transmission generated at least one overcurrent event");
            
            message += "\n\tEstimated ST7580 temperature :\t" + ((_estimatedST7580Temperature == 0)
                           ?"T < 70 °C"
                           :(_estimatedST7580Temperature == 1)
                               ?"70 °C < 100 °C"
                               :(_estimatedST7580Temperature == 2)
                                   ?"100 °C < T < 125 °C"
                                   :"T > 125 °C");
            return message + "\nEND OF THE MESSAGE";
        }
    }
}