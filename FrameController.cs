﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Timer = System.Timers.Timer;

namespace ComTerminalProject
{
    public partial class MainWindow
    {
        //Timeouts
        private const int MillisecondsTimeoutIc = 100;//10
        private const int MillisecondsTimeoutAck = 40;//40
        private const int MillisecondsTimeoutSr = 200;//200
        
        static Queue<int> queue = new Queue<int>();
        private QUEUE FRAME = QUEUE.LOOKING_FOR_FRAME;
        private LocalFrame _frame;
        private StatusMessage _statusMessage;
        
        private Thread InterCaracterTimeoutThread = null;
        private Thread AckThread = null;

        private byte _receivedLen = 0;
        private List<byte> _reveivedData;
        private byte _receivedCheckSumPosition = 0;
        private byte[] _receivedChecksum = {0x00, 0x00};

        private void queueAnalizeerElapsed(Object source, System.Timers.ElapsedEventArgs e)
        {

            //TODO T_IC
            //TODO reset on client value
            while (queue.Count > 0)
            {
                if (InterCaracterTimeoutThread != null)
                {
                    if (InterCaracterTimeoutThread.IsAlive)
                    {
                        InterCaracterTimeoutThread.Abort();
                    }
                }
                byte value = (byte) queue.Dequeue();
                switch (FRAME)
                {
                    case QUEUE.LOOKING_FOR_FRAME:
                        switch ((STX) value)
                        {
                            case STX.ACK:
                                WriteToReceiveBoxOnGuiThread("Odebrano: ACK");
                                abortAckThread();
                                break;
                            case STX.NAK:
                                WriteToReceiveBoxOnGuiThread("Odebrano: NAK");
                                abortAckThread();
                                if (_frame != null)
                                {
                                    Port.RtsEnable = true; Port.DtrEnable = true;
                                    Port.Write(_frame.toResend(), 0, _frame.toResend().Length);
                                    Port.RtsEnable = false; Port.DtrEnable = false;
                                }
                                break;
                            case STX.LOCAL:
                                _frame = new LocalFrame();
                                FRAME = QUEUE.LENGTH;
                                break;
                            case STX.STATUS_MESSAGE_FIRST_BYTE:
                                _statusMessage = new StatusMessage();
                                FRAME = QUEUE.STATUS_MESSAGE;
                                break;
                            case STX.RETRANSMITION:
                                _frame = new LocalFrame();
                                FRAME = QUEUE.LENGTH;
                                break;
                            default:
                                WriteToReceiveBox("chuj");//TODO
                                break;
                        }

                        break;
                    case QUEUE.LENGTH:
                        _frame.Length = value;
                        _receivedLen = value;
                        FRAME = QUEUE.CMC;
                        
                        if (_receivedLen > 0) _reveivedData = new List<byte>();
                        
                        break;
                    case QUEUE.CMC:
                        _frame.Cmc = value;
                        FRAME = _frame.Length > 0 ? QUEUE.DATA : QUEUE.CHECKSUM;
                        break;
                    case QUEUE.DATA:
                        if (_receivedLen-- == 0) FRAME = QUEUE.CHECKSUM;
                        _reveivedData.Add(value);
                        //TODO 
                        break;
                    case QUEUE.CHECKSUM:
                        _frame.Checksum[_receivedCheckSumPosition++] = value;

                        if (_receivedCheckSumPosition == 2)
                        {
                            byte[] STATUS = {(_frame.IsValidChecksum()) ? (byte) STX.NAK : (byte) STX.ACK};

                            Port.DtrEnable = true;
                            Port.Write(STATUS, 0, STATUS.Length);
                            Port.DtrEnable = false;

                            FRAME = QUEUE.LOOKING_FOR_FRAME;
                            _receivedCheckSumPosition = 0;


                            
                            
                            string s = "";
                            foreach (var b in _frame.toSend())
                            {
                                s += "" + b;
                            }
                            WriteToReceiveBoxOnGuiThread("DL frame redeived:" + s );
                        }
                        break;
                    case QUEUE.STATUS_MESSAGE:
                        _statusMessage.Set(value);
                        WriteToReceiveBoxOnGuiThread(_statusMessage.ToWrite());
                        FRAME = QUEUE.LOOKING_FOR_FRAME;
                        break;
                    default:
                        break;
                }
                InterCaracterTimeoutThread = new Thread(new ThreadStart(() =>
                {
                    Thread.Sleep(MillisecondsTimeoutIc);
                    WriteToReceiveBoxOnGuiThread("TIMEOUT: inter character!");
                    FRAME = QUEUE.LOOKING_FOR_FRAME;
                }));
                InterCaracterTimeoutThread.Start();
            }
        }

        private void abortAckThread()
        {
            if (AckThread != null)
                if (AckThread.IsAlive)
                {
                    AckThread.Abort();
                    WriteToReceiveBoxOnGuiThread("THREAD ABORTED");
                }
        }

        private void WriteToReceiveBoxOnGuiThread(string text)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (Action) delegate() { WriteToReceiveBox(text); });
        }
    }
}