﻿namespace ComTerminalProject
{
    public enum QUEUE
    {
        LOOKING_FOR_FRAME,

        LENGTH,
        CMC,
        DATA,
        CHECKSUM,
        
        STATUS_MESSAGE
    }
}