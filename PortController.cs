﻿using System.IO.Ports;

namespace ComTerminalProject
{
    public partial class MainWindow
    {
        public SerialPort Port { get; set; }
        private void SetPortFunctionValue(bool value)
        {
            ConnectButton.IsEnabled = !value;
            SendButton.IsEnabled = value;
            SendReset.IsEnabled = value;
            DisconnectButton.IsEnabled = value;
        }

        private void SetPortFunctionValue()
        {
            ConnectButton.IsEnabled = false;
            SendButton.IsEnabled = false;
            SendReset.IsEnabled = false;
            DisconnectButton.IsEnabled = false;
        }

        private void SetPortsInPortComboBox()
        {
            PortComboBox.Items.Clear();
            string[] portNames = SerialPort.GetPortNames();
            bool portFunctionsValue = (portNames.Length != 0);
            SetPortFunctionValue();
            if (!portFunctionsValue)
            {
                PortComboBox.Items.Add("No available");
                PortComboBox.SelectedIndex = 0;
                PortComboBox.IsEditable = false;
                return;
            }

            foreach (var port in portNames)
            {
                PortComboBox.Items.Add(port);
            }

            ConnectButton.IsEnabled = true;
            PortComboBox.IsEditable = true;
            SetPortFunctionValue();
        }
    }
}