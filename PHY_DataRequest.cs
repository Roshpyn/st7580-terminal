﻿using System;
using System.Collections.Generic;

namespace ComTerminalProject
{
    class PHY_DataRequest
    {
        private List<byte> requestData;

        public PHY_DataRequest(byte[] data,
            RequestData.frequencySet frequency,
            RequestData.GainSelector gainSelector,
            RequestData.FrameModulation frameModulation,
            RequestData.ZeroCrossingSynchronization ZCS)
        {
            requestData = new List<byte>();
            requestData.Add((byte) ((byte) frequency | (byte) gainSelector | (byte) frameModulation | (byte) ZCS));
            requestData.Add(0x00); //TX gain
            foreach (var byteData in data)
            {
                requestData.Add(byteData);
            }
        }

        public PHY_DataRequest(byte[] data, 
            byte[] TxFrequency, 
            RequestData.frequencyOverwrite overwrite,
            RequestData.frequencySet frequency, 
            RequestData.GainSelector gainSelector,
            RequestData.FrameModulation frameModulation, 
            RequestData.ZeroCrossingSynchronization ZCS)
        {
            List<byte> requestData = new List<byte>();
            requestData.Add((byte) (0b_1000_0000 | (byte) overwrite | (byte) frequency | (byte) gainSelector |
                                    (byte) frameModulation | (byte) ZCS));
            requestData.Add(TxFrequency[0]);
            requestData.Add(TxFrequency[1]);
            requestData.Add(TxFrequency[2]);
            requestData.Add(0x00); //TX gain
            foreach (var byteData in data)
            {
                requestData.Add(byteData);
            }
        }

        public string toSend()
        { 
            string s = ""; 
            requestData.ForEach(b => s+=b + " " );
            return s;
        }
    }

    public enum MibObject
    {
        High = 0x014FF0,
        Low = 0x011940
    }

    internal class RequestData
    {
        public enum frequencyOverwrite
        {
            YES = 0b_0100_0000,
            NO = 0x00
        }

        public enum frequencySet
        {
            LOW = 0x00,
            HIGHT = 0b_0010_0000
        }

        public enum GainSelector
        {
            TX_gain_set_as_in_PHY_Config_MIB_object = 0x00,
            TX_gain_is_specified_in_the_following_TX_gain_byte = 0b_0001_0000
        }

        public enum FrameModulation
        {
            //B-PSK 1: Q-PSK 2: 8-PSK 3: B-FSK 4: B-PSK coded 5:
            //Q-PSK coded 6: Reserved 7: B-PSK coded with Peak Noise Avoidance
            //          0b0000XXX0
            B_PSK = 0b_0000_0000,
            Q_PSK = 0b_0000_0010,
            _8_PSK = 0b_0000_0100,
            B_FSK = 0b_0000_0110,
            B_PSK_coded = 0b_0000_1000,
            Q_PSK_coded = 0b_0000_1010,
            B_PSK_coded_with_Peak_Noise_Avoidance = 0b_0000_1110
        }

        public enum ZeroCrossingSynchronization
        {
            AfterZC = 0x01,
            INSTANT = 0x00
        }
    }
}

