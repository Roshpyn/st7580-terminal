﻿using System;
using System.Collections.Generic;

namespace ComTerminalProject
{
    public class LocalFrame
    {
        public byte Length { get; set; }
        public byte Cmc { get; set; }
        public byte[] Data { get; set; }
        public byte[] Checksum { get; set; }

        public LocalFrame()
        {
            Data = new byte[255];
            Checksum = new byte[2];
        }

        public LocalFrame(byte length, byte cmc, byte[] data)
        {
            Length = length;
            Cmc = cmc;
            Data = data;
            Checksum = calculateChecksum();
        }

        private byte[] calculateChecksum()
        {
            short sum = Length;
            sum += Cmc;
            foreach (var b in Data) {sum += b;}
            byte[] bytes =
            {
                (byte) sum,
                (byte) (sum << 8)
            }; 
            return bytes;
        }

        public byte[] toSend() => toBytesList(0x02).ToArray();
        public byte[] toResend() => toBytesList(0x03).ToArray();

        private List<byte> toBytesList( byte stx)
        {
            List<byte> data = new List<byte>();
            data.Add(stx);
            data.Add(Length);
            data.Add(Cmc);
            if (Length > 0)
            {
                foreach (var b in Data)
                {
                    data.Add(b);
                }
            }

            data.Add(Checksum[0]);
            data.Add(Checksum[1]);
            return data;
        }
        public bool IsValidChecksum()
        {
            byte[] localChecsum = calculateChecksum();
            if (Checksum.Equals(localChecsum))
            {
                return true;
            }
            return false;
        }
    }
}