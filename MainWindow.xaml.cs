﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using System.Windows.Threading;
using Timer = System.Timers.Timer;

namespace ComTerminalProject
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow
    {
        private Timer _timer;
        private Timer _queueAnalizeer = new Timer();
        public MainWindow()
        {
            Port = null;
            InitializeComponent();
            SetPortsInPortComboBox();
//            var queueAnalizeer = new Timer();
            _queueAnalizeer.Interval = 10;
            _queueAnalizeer.AutoReset = true;
            _queueAnalizeer.Elapsed += queueAnalizeerElapsed;

            ConnectButton.IsEnabled = false;
            DisconnectButton.IsEnabled = false;
            SendButton.IsEnabled = false;
            SendReset.IsEnabled = false;
            _queueAnalizeer.Enabled = true;
        }

        // menu items 
        private void AppClose_OnClick(object sender, RoutedEventArgs e)
        {
            if (Port != null && Port.IsOpen) Port.Close();
            Close();
        }


        private void About_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Author : Łukasz Szadowski \n" +
                            "Version: 0.5 ",
                "About",
                MessageBoxButton.OK);
        }

        // buttons onclicks
        

        private void SendButton_OnClick(object sender, RoutedEventArgs e)
        {
            //TODO
            PHY_DataRequest phyDataRequest = null;
            if (ModeButtonPhy.IsChecked != null && (bool) ModeButtonPhy.IsChecked)
            {
                List<byte> bytes = new List<byte>();
                foreach (var character in SendBox.Text.ToCharArray())
                {
                    bytes.Add((byte)character);
                }
                phyDataRequest = new PHY_DataRequest(bytes.ToArray(),
                    RequestData.frequencySet.HIGHT,
                    RequestData.GainSelector.TX_gain_set_as_in_PHY_Config_MIB_object,
                    RequestData.FrameModulation.B_PSK_coded_with_Peak_Noise_Avoidance,
                    RequestData.ZeroCrossingSynchronization.INSTANT);
                
            }
            
            
            if (RadioButtonAscii.IsChecked == null || (bool) !RadioButtonAscii.IsChecked) return;
            if ((bool) RadioButtonAscii.IsChecked)
            {
                Port.Write(SendBox.Text);
                WriteToReceiveBox("Sent as ASCII: \"" + phyDataRequest.toSend() + "\"");
                return;
            }

            if (RadioButtonHex.IsChecked != null && (bool) RadioButtonHex.IsChecked)
            {
                //TODO
            }
        }

        private void ConnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            WriteToReceiveBox("Connecting to: " + PortComboBox.SelectedItem);
            SetPortFunctionValue(true);
            PortComboBox.IsEditable = false;

            var name = PortComboBox.SelectedItem.ToString();
            Port = new SerialPort(name, 57600);
            try
            {
                Port.Open();
                _timer = new Timer {Interval = 60.0};
                _timer.Elapsed += (o, args) =>
                {
                    while (Port.IsOpen && Port.BytesToRead > 0)
                    {
                        queue.Enqueue( Port.ReadByte());
                    }
                };
                _timer.AutoReset = true;
                _timer.Enabled = true;
            }
            catch (Exception)
            {
                WriteToReceiveBox("ERROR: Cannot open port");
                SetPortFunctionValue(false);
            }
        }

        private void DisconnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (Port == null || !Port.IsOpen)
            {
                WriteToReceiveBox("Port isn't open.");
                return;
            }

            Port.Close();
            WriteToReceiveBox("Port disconected");
            SetPortFunctionValue(false);
            if (_timer!= null) _timer.Enabled = false;
        }

        private void RefreshButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (Port != null)
            {
                if (Port.IsOpen)
                {
                    Port.Close();
                    WriteToReceiveBox("Disconnected to reset.");
                }
            }

            SetPortsInPortComboBox();
            PortComboBox.IsEditable = true;
            _timer.Enabled = false;
        }

        private void PortComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Port != null)
                if (Port.IsOpen)
                    return;
            ConnectButton.IsEnabled = true;
        }

        private void SendReset_OnClick(object sender, RoutedEventArgs e)
        {
            if (Port == null || !Port.IsOpen) return;
            _frame = new LocalFrame(0x00,0x3c,new byte[0]);
            byte[] resetBytes = _frame.toSend();
            
            WriteToReceiveBox("Sending reset.");
            Port.RtsEnable = true; Port.DtrEnable = true;
            Port.Write(resetBytes, 0, resetBytes.Length);
            Port.RtsEnable = false; Port.DtrEnable = false;
            
            AckThread = new Thread(new ThreadStart(() =>
            {
                Thread.Sleep(MillisecondsTimeoutAck);
                WriteToReceiveBoxOnGuiThread("TIMEOUT: Acknowledgement not received");
                _queueAnalizeer.Stop();
                FRAME = QUEUE.LOOKING_FOR_FRAME;
                _frame = new LocalFrame();
                _queueAnalizeer.Start();
            }));
            AckThread.Start();
        }

        private void WriteToReceiveBox(string s)
        {
            ReceiveBox.Text += s + "\r\n";
        }
    }
}