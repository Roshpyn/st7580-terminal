﻿namespace ComTerminalProject
{
    public enum STX
    {
        ACK = 0x06,
        NAK = 0x15,
        
        LOCAL = 0x02,
        RETRANSMITION = 0x03,
        STATUS_MESSAGE_FIRST_BYTE = 0x3F
    }
}