﻿namespace ComTerminalProject
{
    public class Command
    {
        public enum RequestCommand
        {
            BIO_ResetRequest = 0x3C,
            MIB_WriteRequest = 0x08,
            MIB_ReadRequest = 0x0C,
            MIB_EraseRequest = 0x10,
            PingRequest = 0x2C,
            PHY_DataRequest = 0x24,
            DL_DataRequest = 0x50,
            SS_DataRequest = 0x54,
        }
        public enum ConfirmCommand
        {
            BIO_ResetConfirm = 0x3D,
            MIB_WriteConfirm = 0x09,
            MIB_ReadConfirm = 0x0D,
            MIB_EraseConfirm = 0x11,
            PingConfirm = 0x2D,
            PHY_DataConfirm = 0x25,
            DL_DataConfirm = 0x51,
            SS_DataConfirm = 0x55
        }
        public enum ErrorCommand
        {
            BIO_ResetError = 0x3F,
            MIB_WriteError = 0x0B,
            MIB_ReadError = 0x0F,
            MIB_EraseError = 0x13,
            PHY_DataError = 0x27,
            DL_DataError = 0x53,
            SS_DataError = 0x57,
            CMD_SyntaxError = 0x36
   
        }
        public enum ErrorData
        {
            WPL = 0x02,
            WPV =  0x03,
            Busy = 0x04, 
            ThermalError = 0x0B,
            GeneralError = 0xFF
            /*  Wrong parameter length (WPL) 02h    Data field length in the previous request was wrong
                Wrong parameter value (WPV)  03h    At least one of the parameters values in the previous request was invalid.
                Busy                         04h    System busy, operation couldn’t be performed.
                Thermal error                0Bh    Device internal temperature within threshold 4, ST7580 refused to transmit.
                General error                FFh    Generic error code
            */
        }
        public enum IndicationCommand
        {
            /*  BIO_ResetIndication (Section 3.3.3)    3Eh 
                PHY_DataIndication (Section 3.3.14)    26h
                DL_DataIndication (Section 3.3.17)     52h
                DL_SnifferIndication (Section 3.3.18)  5Ah
                SS_DataIndication (Section 3.3.21)     56h
                SS_SnifferIndication (Section 3.3.22)  5Eh
            */
            Reset = 0x3E,
            PHY_Data = 0x26,
            DL_Data = 0x52,
            DL_Sniffer = 0x5A,
            SS_Data = 0x56,
            SS_Sniffer = 0x5E
        }
    }
}